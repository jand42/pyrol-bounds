import ROL

params_dict = {
    "Step": {
        "Line Search": {
            "Descent Method": {
                "Type": "Quasi-Newton Method"
            }
        },
        "Type": "Interior Point",
        # "Type": "Line Search",
        "Interior Point": {
            "Initial Barrier Penalty": 0.01,
            "Barrier Penalty Reduction Factor": 0.15,
            "Minimum Barrier Penalty": 1e-8
        },
        "Composite Step": {
            "Output Level": 0
        }
    },
    "Status Test": {
        "Gradient Tolerance" :1e-12,
        "Step Tolerance" :1e-16,
        "Constraint Tolerance" :1e-12,
        "Iteration Limit" :10
    }
}


class InequalityConstraint(ROL.Constraint):
    def __init__(self):
        super().__init__()

    def value(self, cvec, x, tol):
        cvec[0] = -x[0] ** 2 - 2.0 * x[1] ** 2 - 4.0 * x[2] ** 2 + 48.0

    def applyJacobian(self, jv, v, x, tol):
        jv[0] = -2.0 * x[0] * v[0] - 4.0 * x[1] * v[1] - 8.0 * x[2] * v[2]
  
    def applyAdjointJacobian(self, jv, v, x, tol):
        jv[0] = -2.0 * x[0] * v[0]
        jv[1] = -4.0 * x[1] * v[0]
        jv[2] = -8.0 * x[2] * v[0]


class Objective(ROL.Objective):
    def __init__(self):
        super().__init__()

    def value(self, x, tol):
        return -x[0] * x[1] * x[2]

    def gradient(self, g, x, tol):
        g[0] = -x[1] * x[2]
        g[1] = -x[0] * x[2]
        g[2] = -x[0] * x[1]

obj = Objective()
x = ROL.StdVector(3)
x[0] = 1.0
x[1] = 1.0
x[2] = 1.0

l = ROL.StdVector(3)
l[0] = 0.0
l[1] = 0.0
l[2] = 0.0

con = InequalityConstraint()

ilower = ROL.StdVector(3)
ilower[0] = -100.0
ilower[1] = -100.0
ilower[1] = -100.0

iupper = ROL.StdVector(3)

iupper[0] = 100.0
iupper[1] = 100.0
iupper[1] = 100.0
ibnd = ROL.Bounds(ilower, iupper, 1.0)

params = ROL.ParameterList(params_dict, "Parameters")
problem = ROL.OptimizationProblem(obj, x, icon=[con], imul=[l], ibnd=[ibnd])
solver = ROL.OptimizationSolver(problem, params)
solver.solve()
print(x[0], x[1], x[2])

